import React from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import { theme } from "../components/form/theme";
import { useStore } from "../redux/store";

// const store = useStore();

// console.log({ s: true, store });

export const decorators = [
  (Story) => (
    // <Provider store={store}>
    <ThemeProvider theme={theme}>
      <Story />
    </ThemeProvider>
    // </Provider>
  ),
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};
