import { Container } from "@material-ui/core";
import { PageHeader } from "../components/form/PageHeader";
import { BottomNavigationBar } from "../components/form/BottomNavigationBar";
import { FollowupPeriodSelector } from "../components/form/FollowupPeriodSelector";
import { ContactInfo } from "../components/form/ContactInfo";
import { Typography } from "@material-ui/core";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";
import { BasicInfo } from "../redux/basicInfoSlice";
import { useTranslate } from "react-polyglot";

export default function Followup() {
  const basicInfo: BasicInfo = useSelector(
    (state: RootState) => state.basicInfo
  );
  const t = useTranslate();

  return (
    <Container maxWidth="sm">
      <PageHeader
        title={t("suggestFollowup")}
        description={t("suggestFollowupExplainer")}
        bodyStyle={{ textAlign: "initial" }}
      />
      <Typography variant="h4">{t("whenFollowup")}</Typography>
      <FollowupPeriodSelector basicInfo={basicInfo} />
      <ContactInfo basicInfo={basicInfo} />
      <BottomNavigationBar next="/preview" back="/grade" />
    </Container>
  );
}
