import { FC, useEffect } from "react";
import { Box, Container } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import {
  updateShowRequirementsDialog,
  updateShowPowerUserDialog,
  updateShowWelcomeDialog,
} from "../redux/appSlice";
import { BasicInfo } from "../components/form/BasicInfo";
import { BottomNavigationBar } from "../components/form/BottomNavigationBar";
import { PageHeader } from "../components/form/PageHeader";
import { PowerUserDialog } from "../components/form/PowerUserDialog";
import { WelcomeDialog } from "../components/form/WelcomeDialog";
import { RequirementsDialog } from "../components/form/RequirementsDialog";
import { RootState } from "../redux/store";
import { useTranslate } from "react-polyglot";

const Intro: FC = () => {
  const {
    showWelcomeDialog,
    showPowerUserDialog,
    showRequirementsDialog,
    fileLoaded,
  } = useSelector((state: RootState) => state.app);
  const t = useTranslate();
  const dispatch = useDispatch();

  useEffect(() => {
    const isPowerUser = localStorage.getItem("powerUser") && !fileLoaded;
    console.log({ isPowerUser, fileLoaded });
    if (isPowerUser) {
      dispatch(updateShowWelcomeDialog(false));
      dispatch(updateShowRequirementsDialog(false));
      dispatch(updateShowPowerUserDialog(true));
    }
  });

  return (
    <Box>
      <WelcomeDialog open={showWelcomeDialog} />
      <RequirementsDialog open={showRequirementsDialog} />
      <PowerUserDialog open={showPowerUserDialog} />
      {!showWelcomeDialog && !showPowerUserDialog && !showRequirementsDialog && (
        <Container maxWidth="sm">
          <PageHeader title={t("introduction")} />
          <BasicInfo />
          <BottomNavigationBar next="/context" />
        </Container>
      )}
    </Box>
  );
};

export default Intro;
