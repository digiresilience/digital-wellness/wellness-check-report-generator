import { Grid, Container } from "@material-ui/core";
import { RiskSection } from "../components/form/RiskSection";
import { BottomNavigationBar } from "../components/form/BottomNavigationBar";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../redux/store";
import { PageHeader } from "../components/form/PageHeader";
import { SectionSelector } from "../components/form/SectionSelector";
import { Button } from "../components/form/Button";
import { updateShowSectionSelection } from "../redux/appSlice";
import { useTranslate } from "react-polyglot";

export default function Risk() {
  const dispatch = useDispatch();
  const app = useSelector((state: RootState) => state.app);
  const basicInfo = useSelector((state: RootState) => state.basicInfo);
  const areas = useSelector((state: RootState) => state.areas);
  const t = useTranslate();

  return (
    <Container maxWidth="md" style={{ textAlign: "center" }}>
      <SectionSelector />
      <PageHeader
        title={t("riskAssessment")}
        description={t("riskAssessmentExplainer")}
      />
      <Grid container direction="row-reverse">
        <Grid item style={{ textAlign: "right" }}>
          <Button
            variant="secondary"
            onClick={() => dispatch(updateShowSectionSelection(true))}
          >
            + {t("addSections")}
          </Button>
        </Grid>
      </Grid>
      {Object.keys(areas).map((area, i) => (
        <RiskSection
          key={area}
          area={area}
          nextArea={Object.keys(areas)[i + 1]}
          nextSections={areas[Object.keys(areas)[i + 1]]?.sections}
          followup={basicInfo.followup}
          {...areas[area]}
          expandedSections={app.expandedSections}
        />
      ))}

      <BottomNavigationBar next="/grade" back="/context" />
    </Container>
  );
}
