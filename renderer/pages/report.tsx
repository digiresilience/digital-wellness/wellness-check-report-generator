import React from "react";
import { Report as IReport } from "../components/report/Report";
import { useSelector } from "react-redux";
import { RootState } from "../redux/store";

export default function Report() {
  const { basicInfo, concerns, areas } = useSelector(
    (state: RootState) => state
  );

  return <IReport {...{ basicInfo, concerns, areas }} />;
}
