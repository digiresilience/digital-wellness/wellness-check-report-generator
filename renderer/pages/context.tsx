import { Box, Container } from "@material-ui/core";
import { useSelector } from "react-redux";
import { BottomNavigationBar } from "../components/form/BottomNavigationBar";
import { Concern } from "../components/form/Concern";
import { PageHeader } from "../components/form/PageHeader";
import { RootState } from "../redux/store";
import { useTranslate } from "react-polyglot";

export default function Context() {
  const app = useSelector((state: RootState) => state.app);
  const basicInfo = useSelector((state: RootState) => state.basicInfo);
  const concerns = useSelector((state: RootState) => state.concerns);
  const t = useTranslate();

  return (
    <Box>
      <Container maxWidth="md" style={{ textAlign: "center" }}>
        <PageHeader
          title={t("describeContext")}
          description={t("describeContextExplainer")}
        />
        {Object.keys(concerns).map((concern, i) => (
          <Concern
            key={concern}
            concern={concern}
            nextConcern={Object.keys(concerns)[i + 1]}
            expanded={app.expandedConcerns.includes(concern)}
            followup={basicInfo.followup}
            {...concerns[concern]}
          />
        ))}
        <BottomNavigationBar next="/risk" back="/intro" />
      </Container>
    </Box>
  );
}
