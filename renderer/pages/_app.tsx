import Head from "next/head";
import { Layout } from "../components/form/Layout";
import { wrapper } from "../redux/store";
import "../styles/main.css";
import { I18n } from "react-polyglot";
import { RootState } from "../redux/store";
import { CookiesProvider } from "react-cookie";
import { useSelector } from "react-redux";
import "@fontsource/poppins/500.css";
import "@fontsource/poppins/600.css";
import "@fontsource/poppins/700.css";
import "@fontsource/tajawal/500.css";
import "@fontsource/tajawal/700.css";
import "@fontsource/tajawal/900.css";
import en from "../assets/translations/en.json";
import fr from "../assets/translations/fr.json";
import ar from "../assets/translations/ar.json";

function WellnessCheckApp({ Component, pageProps }) {
  const { locale, textDirection } = useSelector(
    (state: RootState) => state.app
  );
  const messages = { en, fr, ar };

  return (
    <CookiesProvider>
      <div dir={textDirection}>
        <Head>
          <title>Digital Wellness Check</title>()
        </Head>
        <I18n locale={locale} messages={messages[locale]}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </I18n>
      </div>
    </CookiesProvider>
  );
}

export default wrapper.withRedux(WellnessCheckApp);
