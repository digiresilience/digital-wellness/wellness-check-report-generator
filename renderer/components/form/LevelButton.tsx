import React, { FC } from "react";
import { Icon } from "@material-ui/core";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import { Grid } from "@material-ui/core";
import { ToggleButton } from "@material-ui/lab";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((_) => ({
  button: {
    border: 0,
    padding: 0,
    margin: 0,
    "&:hover": {
      backgroundColor: `none !important`,
    },
  },
}));

interface Props {
  value: number;
  score: number;
  label: string;
}

export const LevelButton: FC<Props> = ({ value, score, label }) => {
  const styles = useStyles();

  return (
    <ToggleButton className={styles.button} value={value}>
      <Grid container direction="column">
        <Grid item>
          <Icon>
            {score === value ? (
              <RadioButtonCheckedIcon />
            ) : (
              <RadioButtonUncheckedIcon />
            )}
          </Icon>
        </Grid>
        <Grid item>{label}</Grid>
      </Grid>
    </ToggleButton>
  );
};
