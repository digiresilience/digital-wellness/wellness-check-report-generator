import React, { FC } from "react";
import { Grid, Typography } from "@material-ui/core";
import { SeverityIndicator } from "./SeverityIndicator";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { useTranslate } from "react-polyglot";

interface Props {
  score: number;
  followupScore: number;
}

export const MultiSeverityIndicator: FC<Props> = ({ score, followupScore }) => {
  const t = useTranslate();

  return (
    <Grid
      container
      direction="row"
      spacing={1}
      wrap="nowrap"
      style={{ marginTop: -5 }}
    >
      <Grid container item direction="column">
        <Grid item>
          <SeverityIndicator score={score} />
        </Grid>
        <Grid item>
          <Typography variant="h6">{t("previously")}</Typography>
        </Grid>
      </Grid>
      <Grid item>
        <ArrowRightIcon
          style={{ width: 35, height: 35, marginTop: -8, color: "#ccc" }}
        />
      </Grid>
      <Grid container item direction="column">
        <Grid item>
          <SeverityIndicator score={followupScore} />
        </Grid>
        <Grid item>
          <Typography variant="h6">{t("now")}</Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};
