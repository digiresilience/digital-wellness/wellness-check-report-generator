import React, { FC } from "react";
import { Grid, Typography } from "@material-ui/core";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

interface RequirementItemProps {
  title: string;
  description: string;
  Icon: any;
}

export const RequirementItem: FC<RequirementItemProps> = ({
  title,
  description,
  Icon,
}) => {
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  return (
    <Grid item>
      <Grid container wrap="nowrap">
        <Grid item>
          <Icon
            style={{
              width: 40,
              height: 40,
              marginLeft: isRTL ? 25 : 15,
              marginRight: isRTL ? 15 : 25,
            }}
          />
        </Grid>
        <Grid item>
          <Typography variant="h3">{title}</Typography>
          <Typography variant="body1">{description}</Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};
