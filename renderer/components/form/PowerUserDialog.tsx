import React, { FC } from "react";
import {
  Container,
  Grid,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
} from "@material-ui/core";
import { useDispatch } from "react-redux";
import { Button } from "./Button";
import { FileSelector } from "./FileSelector";
import DescriptionIcon from "@material-ui/icons/Description";
import { useTheme } from "@material-ui/core/styles";
import {
  updateShowPowerUserDialog,
  updateShowRequirementsDialog,
} from "../../redux/appSlice";
import { RootState } from "../../redux/store";
import { useSelector } from "react-redux";
import { updateFollowup } from "../../redux/basicInfoSlice";
import { useTranslate } from "react-polyglot";

interface PowerUserDialogProps {
  open: boolean;
}

export const PowerUserDialog: FC<PowerUserDialogProps> = ({ open }) => {
  const { textDirection, fileLoaded } = useSelector(
    (state: RootState) => state.app
  );
  const theme = useTheme();
  const dispatch = useDispatch();
  const t = useTranslate();

  const { faintBlue, softBlue }: any = theme.palette;

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" dir={textDirection}>
      <DialogTitle style={{ height: 80, backgroundColor: faintBlue.main }}>
        <Grid container item direction="column" justify="space-around">
          <Typography variant="h3" style={{ marginTop: 10 }}>
            {t("continueJSON")}
          </Typography>
        </Grid>
      </DialogTitle>
      <DialogContent>
        <Container>
          <Grid container direction="column" spacing={4} alignItems="center">
            <Grid item>
              <DescriptionIcon
                style={{ height: 140, width: 140, color: softBlue.main }}
              />
            </Grid>
            <Grid item>
              <FileSelector showIcon={false} />
            </Grid>
            <Grid container item justify="center" spacing={3}>
              <Grid item>
                <Button
                  disabled={!fileLoaded}
                  variant="primary"
                  style={{ color: "white" }}
                  onClick={() => {
                    dispatch(updateFollowup(false));
                    dispatch(updateShowPowerUserDialog(false));
                  }}
                >
                  {t("editReport")}
                </Button>
              </Grid>
              <Grid item>
                <Button
                  disabled={!fileLoaded}
                  variant="primary"
                  style={{ color: "white" }}
                  onClick={() => {
                    dispatch(updateFollowup(true));
                    dispatch(updateShowPowerUserDialog(false));
                  }}
                >
                  {t("createFollowup")}
                </Button>
              </Grid>
            </Grid>
            <Grid container item direction="row" spacing={3}>
              <Grid item>
                <Button
                  variant="secondary"
                  onClick={async () => {
                    localStorage.removeItem("powerUser");
                    dispatch(updateShowRequirementsDialog(true));
                    dispatch(updateShowPowerUserDialog(false));
                  }}
                >
                  {t("back")}
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
    </Dialog>
  );
};
