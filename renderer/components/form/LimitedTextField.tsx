import React, { FC } from "react";
import { TextField, TextFieldProps } from "formik-material-ui";
import { useTheme } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

export const LimitedTextField: FC<TextFieldProps> = (props) => {
  const theme = useTheme();
  const { gray }: any = theme.palette;
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  return (
    <TextField
      style={{
        width: "100%",
        marginTop: -8,
      }}
      multiline
      fullWidth
      rows={6}
      InputProps={{
        style: {
          marginTop: 20,
          marginBottom: 20,
          marginLeft: isRTL ? 20 : 0,
          marginRight: isRTL ? 0 : 20,
          paddingLeft: isRTL ? 0 : 12,
          paddingRight: isRTL ? 12 : 0,
          backgroundColor: gray.light,
        },
      }}
      InputLabelProps={{
        style: {
          color: gray.dark,
          zIndex: 100,
          paddingTop: 12,
          marginLeft: isRTL ? 0 : 12,
          marginRight: isRTL ? 12 : 0,
          pointerEvents: "none",
        },
      }}
      {...props}
    />
  );
};
