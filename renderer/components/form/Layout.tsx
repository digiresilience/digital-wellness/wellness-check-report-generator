import React, { FC } from "react";
import { Grid } from "@material-ui/core";
import { TopNavigationBar } from "./TopNavigationBar";
import { useRouter } from "next/router";
import { ThemeProvider } from "@material-ui/core/styles";
import { theme } from "./theme";
import { LocaleSelector } from "./LocaleSelector";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";

const headerIndexLookup = {
  "/intro": 0,
  "/context": 1,
  "/risk": 2,
  "/grade": 3,
  "/followup": 4,
  "/preview": 5,
};

export const Layout: FC = ({ children }) => {
  const router = useRouter();
  const selectedIndex = headerIndexLookup[router.pathname];
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  if (selectedIndex >= 0) {
    return (
      <ThemeProvider theme={theme}>
        <TopNavigationBar selectedIndex={selectedIndex} />
        {children}

        <Grid
          container
          item
          direction="row-reverse"
          style={{
            padding: 60,
            paddingLeft: isRTL ? 20 : 0,
            paddingRight: isRTL ? 0 : 20,
          }}
        >
          <LocaleSelector />
        </Grid>
      </ThemeProvider>
    );
  } else {
    return <>{children}</>;
  }
};
