import React, { FC } from "react";
import { Typography, LinearProgress, Grid } from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { update } from "../../redux/basicInfoSlice";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/store";
import { useTheme } from "@material-ui/core/styles";
import { SingleLineTextField } from "./SingleLineTextField";
import { ModeSelector } from "./ModeSelector";
import { FileSelector } from "./FileSelector";
import { LimitedTextField } from "./LimitedTextField";
import { useTranslate } from "react-polyglot";

export const BasicInfo: FC = () => {
  const basicInfo = useSelector((state: RootState) => state.basicInfo);
  const { mode } = useSelector((state: RootState) => state.app);
  console.log({ fu: basicInfo.followup, mode });
  const dispatch = useDispatch();
  const theme = useTheme();
  const { gray }: any = theme.palette;
  const t = useTranslate();

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Formik
        initialValues={basicInfo}
        onSubmit={(values, { setSubmitting }) => {
          dispatch(update(values));
          setSubmitting(false);
        }}
      >
        {({ submitForm, isSubmitting }) => (
          <Form>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <Typography variant="h4">{t("whoFor")} *</Typography>
                <Field
                  component={SingleLineTextField}
                  name="organization"
                  type="text"
                  label={t("nameOrganization")}
                  onBlur={submitForm}
                />
              </Grid>
              <Grid item>
                <Typography variant="h4">{t("defineScope")}. *</Typography>
                <Field
                  component={LimitedTextField}
                  name="scope"
                  type="text"
                  rows={4}
                  label={t("scopeAssessment")}
                  onBlur={submitForm}
                />
              </Grid>
              <Grid item>
                <Typography variant="h4" style={{ marginBottom: 16 }}>
                  {basicInfo.followup
                    ? `${t("whenOriginalCompleted")} *`
                    : `${t("whenCompleted")} *`}
                </Typography>
                <Field
                  component={SingleLineTextField}
                  name="originalAssessmentDate"
                  type="date"
                  onBlur={submitForm}
                  label=""
                  style={{ backgroundColor: gray.light, width: "100%" }}
                />
              </Grid>
              {basicInfo.followup ? (
                <Grid item>
                  <Typography variant="h4" style={{ marginBottom: 16 }}>
                    {t("whenFollowupCompleted")} *
                  </Typography>
                  <Field
                    component={SingleLineTextField}
                    name="followupAssessmentDate"
                    type="date"
                    onBlur={submitForm}
                    label=""
                    style={{ backgroundColor: gray.light, width: "100%" }}
                  />
                </Grid>
              ) : null}
            </Grid>
            {isSubmitting && <LinearProgress />}
          </Form>
        )}
      </Formik>
      <ModeSelector />
      <Grid container direction="column" spacing={2}>
        <Grid item hidden={mode !== "file"}>
          <Typography variant="h4" style={{ marginTop: 20, marginBottom: 18 }}>
            {t("selectJSON")}
            <span style={{ color: gray.main }}> (optional)</span>
          </Typography>
          <FileSelector showIcon={true} />
        </Grid>
      </Grid>
    </MuiPickersUtilsProvider>
  );
};
