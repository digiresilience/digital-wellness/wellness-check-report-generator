import React, { FC } from "react";
import { Box, Grid, Typography } from "@material-ui/core";

interface Props {
  title: string;
  description?: string;
  bodyStyle?: any;
  children?: any;
}

export const PageHeader: FC<Props> = ({
  title,
  description,
  bodyStyle,
  children,
}) => (
  <Box
    style={{
      textAlign: "center",
      minHeight: description ? 130 : 70,
      maxWidth: 600,
      margin: "0 auto",
    }}
  >
    <Typography
      variant="h1"
      style={{
        marginTop: 44,
      }}
    >
      {title}
    </Typography>
    <Grid container direction="row" wrap="nowrap" spacing={2}>
      <Grid item>
        <Typography
          variant="body1"
          style={{
            marginTop: 16,
            marginBottom: 44,
            ...bodyStyle,
          }}
        >
          {description}
        </Typography>
      </Grid>
      <Grid item>{children}</Grid>
    </Grid>
  </Box>
);
