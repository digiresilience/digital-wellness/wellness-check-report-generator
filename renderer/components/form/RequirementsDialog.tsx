import React, { FC } from "react";
import {
  Container,
  Grid,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
} from "@material-ui/core";
import { useDispatch } from "react-redux";
import { session } from "electron";
import {
  updateShowRequirementsDialog,
  updateShowPowerUserDialog,
  updateShowWelcomeDialog,
} from "../../redux/appSlice";
import { Button } from "./Button";
import DescriptionIcon from "@material-ui/icons/Description";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import QueueMusicIcon from "@material-ui/icons/QueueMusic";
import AssignmentIcon from "@material-ui/icons/Assignment";
import { RequirementItem } from "./RequirementItem";
import { useTheme } from "@material-ui/core/styles";
import { useTranslate } from "react-polyglot";
import { RootState } from "../../redux/store";
import { useSelector } from "react-redux";

interface RequirementsDialogProps {
  open: boolean;
}

export const RequirementsDialog: FC<RequirementsDialogProps> = ({ open }) => {
  const { textDirection } = useSelector((state: RootState) => state.app);
  const dispatch = useDispatch();
  const theme = useTheme();
  const t = useTranslate();

  const { faintBlue }: any = theme.palette;

  return (
    <Dialog open={open} fullWidth={true} maxWidth="sm" dir={textDirection}>
      <DialogTitle style={{ height: 80, backgroundColor: faintBlue.main }}>
        <Grid container item direction="column" justify="space-around">
          <Typography variant="h2" style={{ marginTop: 10 }}>
            {t("requirementsExplainer")}
          </Typography>
        </Grid>
      </DialogTitle>
      <DialogContent style={{ padding: "0px 0px 20px 0px" }}>
        <Container maxWidth="sm">
          <Grid
            container
            style={{ marginTop: 30, marginBottom: 30 }}
            spacing={4}
          >
            <RequirementItem
              title={t("reportData")}
              description={t("reportDataExplainer")}
              Icon={DescriptionIcon}
            />
            <RequirementItem
              title={t("time")}
              description={t("timeExplainer")}
              Icon={AccessTimeIcon}
            />
            <RequirementItem
              title={t("wellnessGuide")}
              description={t("wellnessGuideExplainer")}
              Icon={AssignmentIcon}
            />
            <RequirementItem
              title={t("music")}
              description={t("musicExplainer")}
              Icon={QueueMusicIcon}
            />
          </Grid>
          <Grid container justify="flex-end" spacing={3} wrap="nowrap">
            <Grid item xs={2}>
              <Button
                variant="secondary"
                onClick={() => {
                  dispatch(updateShowWelcomeDialog(true));
                  dispatch(updateShowPowerUserDialog(false));
                }}
              >
                {t("back")}
              </Button>
            </Grid>
            <Grid item xs={3}></Grid>
            <Grid item xs={4}>
              <Button
                variant="secondary"
                onClick={async () => {
                  localStorage.setItem("powerUser", "true");

                  console.log({ power: localStorage.getItem("powerUser") });
                  dispatch(updateShowRequirementsDialog(false));
                  dispatch(updateShowPowerUserDialog(true));
                }}
              >
                {t("powerUser")}
              </Button>
            </Grid>
            <Grid item xs={4}>
              <Button
                variant="primary"
                onClick={() => dispatch(updateShowRequirementsDialog(false))}
              >
                {t("ready")}
              </Button>
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
    </Dialog>
  );
};
