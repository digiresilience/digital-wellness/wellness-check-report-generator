import React, { FC } from "react";
import { Formik, Form, Field } from "formik";
import { RadioGroup } from "formik-material-ui";
import { useDispatch } from "react-redux";
import { BasicInfo, updateGradeLevel } from "../../redux/basicInfoSlice";
import { GradeLevelOption } from "./GradeLevelOption";
import { gradeLevels } from "../../utils/helpers";

interface GradeLevelSelectorProps {
  basicInfo: BasicInfo;
}

export const GradeLevelSelector: FC<GradeLevelSelectorProps> = ({
  basicInfo,
}) => {
  const dispatch = useDispatch();
  const levels = gradeLevels();

  return (
    <Formik
      initialValues={{ gradeLevel: `${basicInfo.gradeLevel}` }}
      onSubmit={(values, { setSubmitting }) => {
        dispatch(updateGradeLevel(values.gradeLevel));
        setSubmitting(false);
      }}
    >
      {({ submitForm, isSubmitting }) => (
        <Form>
          <Field component={RadioGroup} name="gradeLevel" onClick={submitForm}>
            <GradeLevelOption
              grade={1}
              name={levels[0].name}
              description={levels[0].explainer}
              submitting={isSubmitting}
            />
            <GradeLevelOption
              grade={2}
              name={levels[1].name}
              description={levels[1].explainer}
              submitting={isSubmitting}
            />
            <GradeLevelOption
              grade={3}
              name={levels[2].name}
              description={levels[2].explainer}
              submitting={isSubmitting}
            />
            <GradeLevelOption
              grade={4}
              name={levels[3].name}
              description={levels[3].explainer}
              submitting={isSubmitting}
            />
            <GradeLevelOption
              grade={5}
              name={levels[4].name}
              description={levels[4].explainer}
              submitting={isSubmitting}
            />
          </Field>
        </Form>
      )}
    </Formik>
  );
};
