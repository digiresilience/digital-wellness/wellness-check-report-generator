import { FC } from "react";
import { Formik, Form, Field } from "formik";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/store";
import { updateLocale } from "../../redux/appSlice";

export const LocaleSelector: FC = () => {
  const locale = useSelector((state: RootState) => state.app.locale);
  const dispatch = useDispatch();

  return (
    <Formik
      initialValues={{ locale }}
      onSubmit={(values, { setSubmitting }) => {
        dispatch(updateLocale(values.locale));
        setSubmitting(false);
      }}
    >
      {({ submitForm }) => (
        <Form>
          <Field
            as="select"
            name="locale"
            onMouseLeave={submitForm}
            style={{
              fontFamily: "Poppins, Tajawal, sans-serif",
              fontSize: 15,
              padding: "4px 8px",
            }}
          >
            <option selected={locale === "en"} value="en">
              English
            </option>
            <option selected={locale === "fr"} value="fr">
              Français
            </option>
            <option selected={locale === "ar"} value="ar">
              العربية
            </option>
          </Field>
        </Form>
      )}
    </Formik>
  );
};
