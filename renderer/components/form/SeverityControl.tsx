import React, { FC } from "react";
import { Field } from "formik";
import { ToggleButtonGroup } from "formik-material-ui-lab";
import { ToggleButton } from "@material-ui/lab";
import { makeStyles, createStyles, useTheme } from "@material-ui/core/styles";
import { useTranslate } from "react-polyglot";
import { RootState } from "../../redux/store";
import { useSelector } from "react-redux";

const buildStyle = ({ textColor, backgroundColor, hoverBackgroundColor }) => ({
  margin: "12px 12px 12px 0px",
  border: "1px solid #999999 !important",
  borderRadius: "3px !important",
  fontWeight: 700,
  width: 70,
  lineHeight: "100%",
  color: textColor,
  backgroundColor: `${backgroundColor} !important`,
  "&:hover": {
    backgroundColor: `${hoverBackgroundColor} !important`,
    color: "#fff",
  },
});

const useStyles = makeStyles((theme: any) => {
  const {
    livelyGreen,
    mustardYellow,
    floridaOrange,
    srirachaRed,
    gray,
    black,
    white,
  } = theme.palette;
  const t = useTranslate();

  return createStyles({
    green: buildStyle({
      textColor: black.main,
      backgroundColor: gray.main,
      hoverBackgroundColor: livelyGreen.main,
    }),
    greenSelected: buildStyle({
      textColor: white.main,
      backgroundColor: livelyGreen.main,
      hoverBackgroundColor: livelyGreen.dark,
    }),
    yellow: buildStyle({
      textColor: black.main,
      backgroundColor: gray.main,
      hoverBackgroundColor: mustardYellow.main,
    }),
    yellowSelected: buildStyle({
      textColor: white.main,
      backgroundColor: mustardYellow.main,
      hoverBackgroundColor: mustardYellow.dark,
    }),
    orange: buildStyle({
      textColor: black.main,
      backgroundColor: gray.main,
      hoverBackgroundColor: floridaOrange.main,
    }),
    orangeSelected: buildStyle({
      textColor: white.main,
      backgroundColor: floridaOrange.main,
      hoverBackgroundColor: floridaOrange.dark,
    }),
    red: buildStyle({
      textColor: black.main,
      backgroundColor: gray.main,
      hoverBackgroundColor: srirachaRed.main,
    }),
    redSelected: buildStyle({
      textColor: white.main,
      backgroundColor: srirachaRed.main,
      hoverBackgroundColor: srirachaRed.dark,
    }),
  });
});

interface SeverityControlProps {
  followup: boolean;
  submitForm: any;
}

export const SeverityControl: FC<SeverityControlProps> = ({
  followup,
  submitForm,
}) => {
  const styles: any = useStyles();
  const t = useTranslate();
  const { textDirection } = useSelector((state: RootState) => state.app);
  const isRTL = textDirection === "rtl";

  return (
    <Field
      component={ToggleButtonGroup}
      name={followup ? "followupScore" : "score"}
      type="checkbox"
      exclusive
      style={{ display: "flex", alignItems: "flex-end" }}
    >
      <ToggleButton
        classes={{ root: styles.green, selected: styles.greenSelected }}
        value={1}
        onClick={submitForm}
        style={{ height: isRTL ? 40 : 20 }}
      >
        {t("lowAbbrev")}
      </ToggleButton>
      <ToggleButton
        classes={{ root: styles.yellow, selected: styles.yellowSelected }}
        value={2}
        onClick={submitForm}
        style={{ height: isRTL ? 46 : 23 }}
      >
        {t("moderateLowAbbrev")}
      </ToggleButton>
      <ToggleButton
        classes={{ root: styles.orange, selected: styles.orangeSelected }}
        value={3}
        onClick={submitForm}
        style={{ height: isRTL ? 52 : 28 }}
      >
        {t("moderateAbbrev")}
      </ToggleButton>
      <ToggleButton
        classes={{ root: styles.red, selected: styles.redSelected }}
        value={4}
        onClick={submitForm}
        style={{ height: isRTL ? 58 : 29 }}
      >
        {t("highAbbrev")}
      </ToggleButton>
    </Field>
  );
};
