import React, { FC } from "react";
import { Grid } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import Typography from "@material-ui/core/Typography";
import { ConcernDetail } from "./ConcernDetail";
import { LevelIndicator } from "./LevelIndicator";
import { MultiLevelIndicator } from "./MultiLevelIndicator";
import { concernDisplayConfig } from "../../utils/helpers";
import { useDispatch } from "react-redux";
import { toggleExpandedConcern } from "../../redux/appSlice";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((_) => ({
  accordion: {
    marginTop: 10,
    marginBottom: 10,
    borderRadius: 0,
    border: 0,
    boxShadow: "none",
    "&::before": {
      display: "none",
    },
    borderBottom: "1px solid lightgray",
  },
  accordionExpanded: {
    boxShadow:
      "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)",
    borderRadius: 4,
    borderBottom: 0,
  },
}));

interface ConcernProps {
  concern: string;
  nextConcern: string;
  score: number;
  followupScore: number;
  notes: string;
  active: boolean;
  expanded: boolean;
  followup: boolean;
}

export const Concern: FC<ConcernProps> = ({
  concern,
  nextConcern,
  score,
  followupScore,
  notes,
  active,
  expanded,
  followup,
}) => {
  const styles = useStyles();
  const dispatch = useDispatch();
  const theme: any = useTheme();
  const { softBlue } = theme.palette;

  if (!softBlue) return null;

  return (
    <Accordion
      classes={{ root: styles.accordion, expanded: styles.accordionExpanded }}
      expanded={expanded}
      onChange={() => dispatch(toggleExpandedConcern(concern))}
    >
      <AccordionSummary id={concern}>
        <Grid container justify="space-between">
          <Grid item>
            <Typography
              variant="h3"
              style={{
                color: softBlue.main,
              }}
            >
              {concernDisplayConfig(concern).name}
            </Typography>
          </Grid>
          <Grid item>
            {followup ? (
              <MultiLevelIndicator
                score={score}
                followupScore={followupScore}
              />
            ) : (
              <LevelIndicator score={score} />
            )}
          </Grid>
        </Grid>
      </AccordionSummary>
      <AccordionDetails>
        <Grid container direction="column">
          <Grid item>
            <ConcernDetail
              concern={concern}
              nextConcern={nextConcern}
              active={active}
              score={score}
              followupScore={followupScore}
              followup={followup}
              notes={notes}
            />
          </Grid>
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
};
