import React, { FC } from "react";
import Link from "next/link";
import {
  AppBar,
  Toolbar,
  Grid,
  Tabs,
  Tab,
  Typography,
} from "@material-ui/core";
import { RootState } from "../../redux/store";
import { useSelector } from "react-redux";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import { DownloadButton } from "./DownloadButton";
import { useTranslate } from "react-polyglot";

const useStyles = makeStyles((theme) => {
  const { softBlue, washedBlue, faintBlue }: any = theme.palette;

  return createStyles({
    appBar: {
      backgroundColor: `${faintBlue.main} !important`,
      boxShadow: "none",
      borderBottom: `1px solid ${washedBlue.main}`,
      padding: 20,
      paddingBottom: 0,
    },
    tabs: {
      margin: "0 auto",
    },
    tabsIndicator: {
      backgroundColor: softBlue.main,
      height: 5,
      width: "70px !important",
      marginLeft: 20,
      marginRight: 20,
    },
    tab: {
      fontWeight: 600,
      color: softBlue.main,
      textTransform: "uppercase",
      fontSize: 12,
      padding: 0,
      margin: 0,
      width: "110px !important",
      minWidth: "100px !important",
    },
  });
});

const LinkTab = ({ label, path }) => {
  const styles = useStyles();
  return (
    <Link href={path}>
      <Tab classes={{ root: styles.tab }} label={label} />
    </Link>
  );
};

interface Props {
  selectedIndex: number;
}

export const TopNavigationBar: FC<Props> = ({ selectedIndex }) => {
  const styles = useStyles();
  const t = useTranslate();

  const organization = useSelector((state: RootState) => {
    return state.basicInfo.organization;
  });
  const followup = useSelector((state: RootState) => {
    return state.basicInfo.followup;
  });

  const handleChange = () => {};

  return (
    <>
      <AppBar classes={{ root: styles.appBar }}>
        <Grid container direction="column" justify="space-between">
          <Grid item>
            <Grid
              container
              wrap="nowrap"
              alignItems="flex-start"
              justify="space-between"
            >
              <Link href="">
                <Grid item>
                  <Grid container wrap="nowrap" spacing={1}>
                    <Grid item>
                      <img
                        style={{ width: 40, height: 40 }}
                        src="/images/wellness-check-logo.png"
                      />
                    </Grid>
                    <Grid item>
                      <Grid container direction="column">
                        <Grid item>
                          <Typography
                            variant="h3"
                            style={{ color: "black", margin: 0, marginTop: -5 }}
                          >
                            {organization ?? t("digitalWellnessCheck")}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <span
                            style={{
                              fontFamily: "Poppins, Tajawal, sans-serif",
                              backgroundColor: "pink",
                              color: "black",
                              margin: 0,
                              borderRadius: 10,
                              fontSize: 10,
                              padding: "3px 6px 3px 6px",
                            }}
                          >
                            {followup ? t("followupReport") : t("newReport")}
                          </span>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Link>
              <Grid item style={{ zIndex: 1000 }}>
                {typeof window !== undefined && <DownloadButton />}
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <Tabs
              classes={{
                root: styles.tabs,
                indicator: styles.tabsIndicator,
              }}
              value={selectedIndex}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              centered
            >
              <LinkTab label={t("intro")} path="/intro" />
              <LinkTab label={t("context")} path="/context" />
              <LinkTab label={t("risk")} path="/risk" />
              <LinkTab label={t("grade")} path="/grade" />
              <LinkTab label={t("followup")} path="/followup" />
              <LinkTab label={t("preview")} path="/preview" />
            </Tabs>
          </Grid>
        </Grid>
      </AppBar>
      <Toolbar style={{ marginBottom: 80 }} />
    </>
  );
};
