import React, { FC, useState } from "react";
import { Typography, Grid, Paper } from "@material-ui/core";
import { Button } from "./Button";
import { useDispatch } from "react-redux";
import { updateFileLoaded } from "../../redux/appSlice";
import { load as loadBasicInfo } from "../../redux/basicInfoSlice";
import { load as loadConcerns } from "../../redux/concernsSlice";
import { load as loadAreas } from "../../redux/areasSlice";
import BusinessIcon from "@material-ui/icons/Business";
import Dropzone from "react-dropzone";
import schema from "../../schema.json";
import Ajv from "ajv";
import { useTranslate } from "react-polyglot";

interface FileSelectorProps {
  showIcon: boolean;
}

export const FileSelector: FC<FileSelectorProps> = ({ showIcon }) => {
  const dispatch = useDispatch();
  const t = useTranslate();
  const [fileName, setFileName] = useState("");

  const loadConfig = (conf: string) => {
    const ajv = new Ajv({ allErrors: true });
    const validate = ajv.compile(schema);
    const valid = validate(conf);

    console.log(validate.errors);

    if (valid) {
      const parsed = JSON.parse(conf);
      dispatch(loadBasicInfo(parsed.basicInfo));
      dispatch(loadConcerns(parsed.concerns));
      dispatch(loadAreas(parsed.areas));
      dispatch(loadBasicInfo(parsed.basicInfo));
      dispatch(updateFileLoaded(true));
    }
  };

  const fileReceived = (files: File[]) => {
    if (files.length > 0) {
      const file = files[0];
      setFileName(file.name);
      const reader = new window.FileReader();
      reader.onload = (event) => {
        const json = event.target.result as string;
        loadConfig(json);
      };
      reader.readAsText(file);
    }
  };

  return (
    <Grid container direction="column">
      <Grid item>
        <Paper variant="outlined" square style={{ padding: 40 }}>
          <Dropzone onDrop={(acceptedFiles) => fileReceived(acceptedFiles)}>
            {({ getRootProps, getInputProps }) => (
              <section>
                <div {...getRootProps()}>
                  <input {...getInputProps()} />
                  <Grid container alignItems="center" alignContent="center">
                    <Grid item xs={3} hidden={!showIcon}>
                      <BusinessIcon
                        style={{
                          width: 80,
                          height: 80,
                        }}
                      />
                    </Grid>
                    <Grid item xs={showIcon ? 9 : 12}>
                      <Grid
                        container
                        spacing={2}
                        direction="column"
                        alignContent="center"
                        alignItems="center"
                      >
                        <Grid item>
                          <Typography
                            variant="body1"
                            style={{ textAlign: "center", paddingTop: 10 }}
                          >
                            {t("dragDropJSON")}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography
                            variant="caption"
                            style={{ color: "#aaa" }}
                          >
                            ───── {t("or")} ─────
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Button variant="secondary">
                            {t("selectComputer")}
                          </Button>
                        </Grid>
                        <Grid>
                          <Typography variant="h6">{fileName}</Typography>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </div>
              </section>
            )}
          </Dropzone>
        </Paper>
      </Grid>
    </Grid>
  );
};
