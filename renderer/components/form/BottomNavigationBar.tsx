import React, { FC } from "react";
import { Grid } from "@material-ui/core";
import Link from "next/link";
import { Button } from "./Button";
import { useTranslate } from "react-polyglot";

interface Props {
  next?: string;
  back?: string;
}

export const BottomNavigationBar: FC<Props> = ({ next, back }) => {
  const t = useTranslate();

  return (
    <Grid
      container
      direction="row-reverse"
      spacing={2}
      style={{ paddingTop: 20, paddingBottom: 20 }}
    >
      <Grid item>
        {next && (
          <Link href={next}>
            <Button variant="primary">{t("next")}</Button>
          </Link>
        )}
      </Grid>
      <Grid item>
        {back && (
          <Link href={back}>
            <Button variant="secondary">{t("back")}</Button>
          </Link>
        )}
      </Grid>
    </Grid>
  );
};
