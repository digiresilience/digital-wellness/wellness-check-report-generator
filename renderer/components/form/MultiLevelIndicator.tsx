import React, { FC } from "react";
import { Grid, Typography } from "@material-ui/core";
import { LevelIndicator } from "./LevelIndicator";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { useTranslate } from "react-polyglot";

interface Props {
  score: number;
  followupScore: number;
}

export const MultiLevelIndicator: FC<Props> = ({ score, followupScore }) => {
  const t = useTranslate();

  return (
    <Grid
      container
      direction="row"
      spacing={1}
      wrap="nowrap"
      style={{ marginTop: -5 }}
    >
      <Grid container item direction="column">
        <Grid item>
          <LevelIndicator score={score} />
        </Grid>
        <Grid item>
          <Typography variant="h6" style={{ marginTop: -6 }}>
            {t("previously")}
          </Typography>
        </Grid>
      </Grid>
      <Grid item>
        <ArrowRightIcon
          style={{ width: 35, height: 35, marginTop: -5, color: "#ccc" }}
        />
      </Grid>
      <Grid container item direction="column">
        <Grid item>
          <LevelIndicator score={followupScore} />
        </Grid>
        <Grid item>
          <Typography variant="h6" style={{ marginTop: -6 }}>
            {t("now")}
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};
