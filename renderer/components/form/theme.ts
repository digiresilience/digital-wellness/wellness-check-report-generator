// @ts-nocheck
import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  // direction: "rtl",
  palette: {
    white: {
      main: "#FFFFFF",
    },
    black: {
      main: "#4A4A4A",
    },
    gray: {
      main: "#D7D7D7",
      light: "#F3F3F3",
      dark: "#7E7E7E",
    },
    livelyGreen: {
      main: "#2EBE60",
      light: "#ABE5BF",
    },
    spinachGreen: {
      main: "#009688",
    },
    hazyGreen: {
      main: "#EDFBF5",
    },
    turquoiseGreen: {
      main: "#00C2B0",
      light: "#C0E6D8",
      dark: "#039D8F",
    },
    mustardYellow: {
      main: "#F3E24B",
      light: "#FAF3B7",
      dark: "#D4C00A",
    },
    floridaOrange: {
      main: "#F69935",
      light: "#FCD6AE",
      dark: "#C66600",
    },
    srirachaRed: {
      main: "#CB190E",
      light: "#EAA39E",
      dark: "#830800",
    },
    softBlue: {
      main: "#283593",
    },
    washedBlue: {
      main: "#DAE1EE",
    },
    faintBlue: {
      main: "#EAEDF4",
    },
    primary: {
      main: "#000",
    },
    secondary: {
      main: "#ffffff",
    },
  },
  typography: {
    fontFamily: "Poppins, Tajawal, sans-serif",
    h1: {
      fontWeight: "bold",
      fontSize: 32,
      lineHeight: "41px",
      letterSpacing: "0.02em",
    },
    h2: {
      fontWeight: "bold",
      fontSize: 24,
      lineHeight: "36px",
    },
    h3: {
      fontWeight: "bold",
      fontSize: 20,
      lineHeight: "30px",
    },
    h4: {
      fontWeight: 600,
      fontSize: 16,
      lineHeight: "24px",
    },
    h5: {
      fontWeight: "bold",
      fontSize: 12,
      lineHeight: "18px",
      textTransform: "uppercase",
    },
    h6: {
      fontWeight: 500,
      fontSize: 10,
      lineHeight: "15px",
      marginTop: 3,
    },
    subtitle1: {},
    subtitle2: {},
    body1: {
      fontFamily: "Roboto, Tajawal, sans-serif",
      fontSize: 16,
      lineHeight: "125%",
    },
    body2: {
      fontFamily: "Roboto, Tajawal, sans-serif",
      fontSize: 13,
      lineHeight: "14px",
      letterSpacing: 0.4,
    },
    button: {},
    caption: {},
    overline: {},
  },
});
