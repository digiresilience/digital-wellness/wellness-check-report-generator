import React, { FC } from "react";
import {
  Typography,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/store";
import { Button } from "./Button";
import { RiskListItem } from "./RiskListItem";
import { updateShowSectionSelection } from "../../redux/appSlice";
import { useTheme } from "@material-ui/core/styles";
import { useTranslate } from "react-polyglot";

export const SectionSelector: FC = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const t = useTranslate();
  const { showSectionSelection, textDirection } = useSelector(
    (state: RootState) => state.app
  );
  const areas = useSelector((state: RootState) => state.areas);
  const { faintBlue }: any = theme.palette;

  return (
    <Dialog open={showSectionSelection} dir={textDirection}>
      <DialogTitle style={{ height: 100, backgroundColor: faintBlue.main }}>
        <Grid container direction="column" justify="space-around">
          <Grid item>
            <Typography variant="h2" style={{ marginTop: 20 }}>
              {t("addSections")}
            </Typography>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent style={{ padding: 12 }}>
        {Object.keys(areas).map((area) => (
          <RiskListItem key={area} area={area} {...areas[area]} />
        ))}
      </DialogContent>
      <DialogActions style={{ padding: 20 }}>
        <Button
          variant="primary"
          onClick={() => dispatch(updateShowSectionSelection(false))}
        >
          {t("done")}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
