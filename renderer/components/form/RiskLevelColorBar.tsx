import React, { FC } from "react";
import { Grid, Typography } from "@material-ui/core";

interface RiskLevelColorBarProps {
  count: number;
  backgroundColor: string;
}

export const RiskLevelColorBar: FC<RiskLevelColorBarProps> = ({
  count,
  backgroundColor,
}) => {
  return (
    <Grid item>
      <Grid
        container
        direction="column-reverse"
        style={{
          marginTop: 4,
          width: 30,
        }}
      >
        <Grid
          item
          style={{
            paddingBottom: 3,
            backgroundColor,
            height: count * 12,
          }}
        ></Grid>
        <Grid item style={{ paddingBottom: 0 }}>
          <Typography
            variant="caption"
            style={{
              color: "black",
              fontWeight: "bold",
            }}
          >
            {count}
          </Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};
