import React, { FC } from "react";
import styles from "./Context.module.css";
import { concernDisplayConfig } from "../../utils/helpers";

interface Props {
  concern: string;
  notes: string;
  active: boolean;
}

export const ContextArea: FC<Props> = ({ concern, notes, active }) => {
  const { Image, name } = concernDisplayConfig(concern);

  if (!active) return null;

  return (
    <div className={styles.contextContainer}>
      <div className={styles.contextHeaderContainer}>
        <Image />
        <h3 className={styles.contextHeader}>{name}</h3>
      </div>
      {notes?.split("\n").map((line, i) => (
        <p key={i} className={styles.bodyText}>
          {line}
        </p>
      ))}
    </div>
  );
};
