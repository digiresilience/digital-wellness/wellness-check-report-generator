import React, { FC } from "react";
import styles from "./ScoreIndicator.module.css";
import { indicatorColors } from "../../utils/helpers";

interface Props {
  score: number;
  label: string;
}

export const ScoreIndicator: FC<Props> = ({ score, label }) => (
  <div className={styles.colorsContainer}>
    <h6 className={styles.colorHeader}>{label}</h6>
    {indicatorColors.map((color, index) => (
      <div
        key={color}
        className={styles.colorContainer}
        style={{
          backgroundColor: color,
          opacity: score === index + 1 ? 1.0 : 0.2,
          width: index * 5 + 25,
          height: index * 5 + 25,
        }}
      />
    ))}
  </div>
);
