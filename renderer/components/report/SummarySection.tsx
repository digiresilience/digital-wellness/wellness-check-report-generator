import React, { FC } from "react";
import styles from "./SummarySection.module.css";
import { areaDisplayConfig } from "../../utils/helpers";
import { Section } from "../../redux/areasSlice";
import { ScoreIndicator } from "./ScoreIndicator";
import { useTranslate } from "react-polyglot";

interface Props extends Section {
  section: string;
  followup: boolean;
}

export const SummarySection: FC<Props> = (props) => {
  const {
    section,
    score,
    followupScore,
    explanation,
    reasonToChange,
    recommendations,
    milestones,
    active,
    followup,
  } = props;
  const t = useTranslate();

  if (!active) return null;

  if (!explanation && !reasonToChange && !recommendations && !milestones) {
    return null;
  }

  return (
    <div className={styles.summarySectionContainer}>
      <ScoreIndicator score={score} label={followup ? "Previous" : ""} />
      {followup && (
        <ScoreIndicator score={followupScore} label={t("current")} />
      )}
      <div className={styles.detailsContainer}>
        <h3 className={styles.areaTitle}>{areaDisplayConfig(section).name}</h3>
        <div className={styles.commentsContainer}>
          <div className={styles.commentContainer}>
            <p className={styles.subareaHeader}>{t("explanation")}</p>
            {explanation?.split("\n").map((line, i) => (
              <p key={i} className={styles.bodyText}>
                {line}
              </p>
            ))}
          </div>
          <div className={styles.commentContainer}>
            <p className={styles.subareaHeader}>{t("reasonToChange")}</p>
            {reasonToChange?.split("\n").map((line, i) => (
              <p key={i} className={styles.bodyText}>
                {line}
              </p>
            ))}
          </div>
          <div className={styles.commentContainer}>
            <p className={styles.subareaHeader}>{t("recommendations")}</p>
            {recommendations?.split("\n").map((line, i) => (
              <p key={i} className={styles.bodyText}>
                {line}
              </p>
            ))}
          </div>
          <div className={styles.commentContainer}>
            <p className={styles.subareaHeader}>{t("milestones")}</p>
            {milestones?.split("\n").map((line, i) => (
              <p key={i} className={styles.bodyText}>
                {line}
              </p>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
