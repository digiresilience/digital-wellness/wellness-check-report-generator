import React, { FC } from "react";
import styles from "./SummaryHeader.module.css";
import { format, parse } from "date-fns";
import { useTranslate } from "react-polyglot";

interface Props {
  title: string;
  date: string;
}

export const SummaryHeader: FC<Props> = ({ title, date }) => {
  const t = useTranslate();

  return (
    <div className={styles.summaryHeaderContainer}>
      <div>
        <h5 className={styles.summaryTitle}>{t("summary")}</h5>
        <h2 className={styles.sectionTitle}>{title}</h2>
      </div>
      <div className={styles.dateContainer}>
        <p className={styles.dateField}>
          {format(
            parse(
              date ?? format(new Date(), "yyyy-MM-dd"),
              "yyyy-MM-dd",
              new Date()
            ),
            "MMM dd, yyyy"
          )}
        </p>
      </div>
    </div>
  );
};
