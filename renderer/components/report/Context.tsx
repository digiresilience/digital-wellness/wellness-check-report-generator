import React, { FC } from "react";
import { SummaryHeader } from "./SummaryHeader";
import styles from "./Context.module.css";
import { ContextArea } from "./ContextArea";
import { useTranslate } from "react-polyglot";

interface Props {
  concerns: any;
  date: string;
}

export const Context: FC<Props> = ({ concerns, date }) => {
  const t = useTranslate();

  return (
    <div className={styles.contextsContainer}>
      <SummaryHeader title={t("context")} date={date} />
      {Object.keys(concerns).map((concern) => (
        <ContextArea key={concern} concern={concern} {...concerns[concern]} />
      ))}
    </div>
  );
};
