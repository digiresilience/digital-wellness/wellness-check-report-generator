import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

export const appSlice = createSlice({
  name: "app",
  initialState: {
    mode: "form",
    locale: "en",
    textDirection: "ltr",
    fileLoaded: false,
    showWelcomeDialog: true,
    showRequirementsDialog: false,
    showPowerUserDialog: false,
    showSectionSelection: true,
    expandedConcerns: [],
    expandedSections: [],
  },
  reducers: {
    updateFileLoaded: (state, action: PayloadAction<boolean>) => {
      state.fileLoaded = action.payload;
    },
    updateLocale: (state, action: PayloadAction<string>) => {
      state.locale = action.payload;
      state.textDirection = state.locale === "ar" ? "rtl" : "ltr";
    },
    updateMode: (state, action: PayloadAction<string>) => {
      state.mode = action.payload;
    },
    updateShowSectionSelection: (state, action: PayloadAction<boolean>) => {
      state.showSectionSelection = action.payload;
    },
    updateShowWelcomeDialog: (state, action: PayloadAction<boolean>) => {
      state.showWelcomeDialog = action.payload;
    },
    updateShowRequirementsDialog: (state, action: PayloadAction<boolean>) => {
      state.showRequirementsDialog = action.payload;
    },
    updateShowPowerUserDialog: (state, action: PayloadAction<boolean>) => {
      state.showPowerUserDialog = action.payload;
    },
    toggleExpandedConcern: (state, action: PayloadAction<string>) => {
      const concern = action.payload;
      if (state.expandedConcerns.includes(concern)) {
        state.expandedConcerns = state.expandedConcerns.filter(
          (i) => i !== concern
        );
      } else {
        state.expandedConcerns = [...state.expandedConcerns, concern];
      }
    },
    toggleExpandedSection: (state, action: PayloadAction<any>) => {
      const { area, section } = action.payload;
      const key = `${area}-${section}`;
      if (state.expandedSections.includes(key)) {
        state.expandedSections = state.expandedSections.filter(
          (i) => i !== key
        );
      } else {
        state.expandedSections = [...state.expandedSections, key];
      }
    },
  },
  extraReducers: {
    [HYDRATE]: (_, action) => action.payload.app,
  },
});

export const {
  updateFileLoaded,
  updateMode,
  updateLocale,
  updateShowSectionSelection,
  updateShowWelcomeDialog,
  updateShowRequirementsDialog,
  updateShowPowerUserDialog,
  toggleExpandedConcern,
  toggleExpandedSection,
} = appSlice.actions;

export default appSlice.reducer;
