import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

export interface BasicInfo {
  followup: boolean;
  organization: string;
  scope: string;
  originalAssessmentDate: string;
  followupAssessmentDate: string;
  grade: number;
  gradeLevel: number;
  gradeLevelOverride: boolean;
  followupMonths: number;
  organizationContactName: string;
  organizationContactInfo: string;
  supportContactName: string;
  supportContactInfo: string;
}

const initialState = {
  followup: false,
  organization: null,
  scope: null,
  originalAssessmentDate: null,
  followupAssessmentDate: null,
  grade: 0,
  gradeLevel: 0,
  gradeLevelOverride: false,
  followupMonths: 3,
  organizationContactName: "",
  organizationContactInfo: "",
  supportContactName: "",
  supportContactInfo: "",
};

export const basicInfoSlice = createSlice({
  name: "basicInfo",
  initialState,
  reducers: {
    update: (state, action: PayloadAction<BasicInfo>) => {
      Object.assign(state, action.payload);
    },
    updateGradeLevel: (state, action: PayloadAction<string>) => {
      state.gradeLevel = parseInt(action.payload, 10);
      state.gradeLevelOverride = true;
    },
    updateFollowupMonths: (state, action: PayloadAction<string>) => {
      state.followupMonths = parseInt(action.payload, 10);
    },
    updateFollowup: (state, action: PayloadAction<boolean>) => {
      state.followup = action.payload;
    },
    load: (state, action: PayloadAction<BasicInfo>) => {
      Object.assign(state, action.payload);
    },
    clear: (state) => {
      Object.assign(state, initialState);
    },
  },
  extraReducers: {
    [HYDRATE]: (_, action) => action.payload.basicInfo,
  },
});

export const {
  update,
  updateGradeLevel,
  updateFollowupMonths,
  updateFollowup,
  load,
  clear,
} = basicInfoSlice.actions;

export default basicInfoSlice.reducer;
