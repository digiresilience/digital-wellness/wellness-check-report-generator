import React from "react";
// import { Summary } from "../../../components/report/Summary";
// import { loremIpsum } from "lorem-ipsum";

// export default {
//   title: "Report/Composite/Summary",
//   component: Summary,
// };

// const Template = (args) => <Summary {...args} />;
// const explanation = loremIpsum({ count: 3 });
// const reasonToChange = loremIpsum({ count: 2 });
// const recommendations = loremIpsum({ count: 4 });
// const milestones = loremIpsum({ count: 1 });

// export const One = Template.bind({});
// One.args = {
//   name: "Some section",
//   date: "2020-10-10",
//   sections: [
//     {
//       name: "Subsection",
//       score: 1,
//       explanation,
//       reasonToChange,
//       recommendations,
//       milestones,
//     },
//   ],
// };

// export const Two = Template.bind({});
// Two.args = {
//   name: "Some section",
//   date: "2020-10-10",
//   sections: [
//     {
//       name: "Subsection",
//       score: 2,
//       explanation,
//       reasonToChange,
//       recommendations,
//       milestones,
//     },
//   ],
// };

// export const Three = Template.bind({});
// Three.args = {
//   name: "Some section",
//   date: "2020-10-10",
//   sections: [
//     {
//       name: "Subsection",
//       score: 3,
//       explanation,
//       reasonToChange,
//       recommendations,
//       milestones,
//     },
//   ],
// };

// export const Four = Template.bind({});
// Four.args = {
//   name: "Some section",
//   date: "2020-10-10",
//   sections: [
//     {
//       name: "Subsection",
//       score: 4,
//       explanation,
//       reasonToChange,
//       recommendations,
//       milestones,
//     },
//   ],
// };

// export const Followup = Template.bind({});
// Followup.args = {
//   name: "Some section",
//   date: "2020-10-10",
//   sections: [
//     {
//       name: "Subsection",
//       score: 1,
//       followupScore: 3,
//       explanation,
//       reasonToChange,
//       recommendations,
//       milestones,
//     },
//   ],
// };
